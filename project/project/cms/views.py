from django.shortcuts import render, get_object_or_404
from django.db.models import Q
from django.template import loader, Context
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Story, Category


def home(request):
    _story_list = Story.objects.all()
    paginator = Paginator(_story_list, 2)   
    page = request.GET.get('page')
    try:
        _story_list = paginator.page(page)
    except PageNotAnInteger:
        #If page not an integer, deliver first page.
        _story_list = paginator.page(1)
    except EmptyPage:
        #If page out of range, deliver last page.
        _story_list = paginator.page(paginator.num_pages)
        
    return render(request, 'story_list.html', {'story_list': _story_list})
    

def story_detail(request, slug):
    _story_detail = get_object_or_404(Story, slug = slug)
    return render(request, 'story_detail.html', {'story_detail': _story_detail})
  
  
def category(request, slug):
    _category = get_object_or_404(Category, slug = slug)
    _story_list = Story.objects.filter(category = _category)
    _context_dic = {
        'story_list': _story_list,
        'heading'   : _category.label,
    }
    return render(request, 'story_list.html', _context_dic)
    
    
def search(request):
    term = request.GET.get('q')
    _story_list = Story.objects.filter(Q(title__contains = term) | Q(html_content__contains = term)) 
    _context_dic = {
        'story_list': _story_list,
        'heading'   : 'Search result',
    }
    return render(request, 'story_list.html', _context_dic)    