from django.conf.urls import url, include
from . import views


urlpatterns = [
    url(r'^$', views.home),
    url(r'^search/',views.search),
    url(r'^(?P<slug>[-\w]+)/$', views.story_detail, name='cms-story-detail'),
    url(r'^category/(?P<slug>[-\w]+)/$', views.category, name='cms-category'),   
]